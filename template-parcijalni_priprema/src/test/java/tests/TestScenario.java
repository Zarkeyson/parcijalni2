package tests;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.internal.Utils;

import pages.HomePageObject;
import pages.LoginPageObject;
import pages.NavBarPageObject;

public class TestScenario {
	private WebDriver driver;
	private LoginPageObject loginPage;
	private  NavBarPageObject navBar;
	HomePageObject homePage;
	
	
	@BeforeSuite
	public void init(){
		System.setProperty("webdriver.chrome.driver", "chromedriver");
		driver = new ChromeDriver();
		
		loginPage = new LoginPageObject(driver);
		navBar = new NavBarPageObject(driver);
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
		driver.navigate().to("http://localhost:8080/#/login");
	}
	
	@AfterSuite
	public void quit(){
		driver.quit();
	}
	
	@Test(priority = 0)
	public void loginUser() throws InterruptedException {
		loginPage.signIn("admin", "admin");
		String msgString = homePage.getLoginMessage();
		assertEquals(msgString, "You are logged in as user \"admin\".");
		Thread.sleep(3000);
	}
	
	@Test(dependsOnMethods = {"loginUser"})
	public void clickNastavnici(){
		navBar.clickEntities();
		navBar.clickAccount();
		//waitForTitle pogledati
		//da smo tu po title stranice ili po urlu( TO MOGU BITI UNIVERZALNE FUNKCIJE!!!) u utils ih mogu naci
		//Utils.
	}
}
