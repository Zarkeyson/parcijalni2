package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {
	
	public static WebElement waitToBeClickable(WebDriver wd, int interval, By selector){
		//ovo se uvek radi na pocetku a mozemo i prekopirati od nekud
		WebElement we = (new WebDriverWait(wd, interval)).until(ExpectedConditions.elementToBeClickable(selector));
		return we;
	}
	
	//copy paste waitToBeClickable u ove dve ispod
	public static WebElement waitToBeVisible(WebDriver wd, int interval, By selector){
		//ovo se uvek radi na pocetku a mozemo i prekopirati od nekud
		WebElement we = (new WebDriverWait(wd, interval)).until(ExpectedConditions.visibilityOfElementLocated(selector));
		return we;
	}
	
	public static WebElement waitToBePresent(WebDriver wd, int interval, By selector){
		//ovo se uvek radi na pocetku a mozemo i prekopirati od nekud
		WebElement we = (new WebDriverWait(wd, interval)).until(ExpectedConditions.presenceOfElementLocated(selector));
		return we;
	}
	
	public static boolean checkTitle(WebDriver wd, int waitInterval,String title){ //posto proveravamo moramo mu proslediti nesto
		return(new WebDriverWait(wd, waitInterval)).until(ExpectedConditions.titleIs(title));
	}
	
}