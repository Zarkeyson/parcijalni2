package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudentPageObject {
	WebDriver driver;

	public StudentPageObject(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	//ok ova i metoda ispod su ok ali bolje je da sve stavim u jednu (bice mi 3. po redu ovde)
	public WebElement btnNewStudent(){
		WebElement btn = Utils.waitToBeVisible(driver, 2, By.xpath("//button[@ui-sref='studenti.new']"));
		return btn;
	}
	
	public void clickBtn(){
		Utils.waitToBeVisible(driver, 2, By.xpath("//button[@ui-sref='studenti.new']")).click();
	}
	
	//efikasniji nacin (ovako uraditi na testu!!
	public void clickCreateNewStudent(){
		WebElement btn = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ui-sref='studenti.new']")));
		btn.click();
	}
	
	public WebElement listStudent(){
		WebElement table = Utils.waitToBeVisible(driver, 1, By.className("table-responsive"));
		return table;
	}
}
