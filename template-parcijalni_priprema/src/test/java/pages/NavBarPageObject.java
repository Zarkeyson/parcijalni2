package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NavBarPageObject {
	//prvo nam treba (pre logina) Account i Sign in sa navbara!!!
	WebDriver driver;
	
	public NavBarPageObject(WebDriver driver){
		super();
		this.driver = driver;
	}
	
	
	//ovo moje nije dobro, ne treba nam! odma ono da nadje i klikne
	public WebElement getAccount(){
		WebElement acc = Utils.waitToBeClickable(driver, 2, By.xpath("//ul[@class='nav navbar-nav navbar-right']/li[2]")); //moze i id=account-menu
		return acc;
	}
	
	public void clickAccount(){
		Utils.waitToBeClickable(driver, 2, By.id("account-menu")).click();
	}
	
	public void clickSignIn(){
		Utils.waitToBeClickable(driver, 2, By.cssSelector("a[ui-sref='login']")).click();
	}
	
	public void clickEntities(){
		Utils.waitToBeClickable(driver, 2, By.xpath("//ul[@class='nav navbar-nav navbar-right']/li[2]")).click();
	}
	
	public void clickNastavnicis(){
		Utils.waitToBeClickable(driver, 1, By.xpath("//a[@ui-sref='nastavnici']")).click();
	}
	
	public void clickStudentis(){
		Utils.waitToBeClickable(driver, 1, By.xpath("//a[@ui-sref='studenti']")).click();
	}
	
	public void clickPredmetis(){
		Utils.waitToBeClickable(driver, 1, By.xpath("//a[@ui-sref='predmeti']")).click();
	}
	
}
