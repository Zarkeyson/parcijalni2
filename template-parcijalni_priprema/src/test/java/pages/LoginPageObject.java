package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPageObject {
	
	private WebDriver driver;
	
	
	
	
	public LoginPageObject(WebDriver driver) {
		super();
		this.driver = driver;
	}

	
	public WebElement getUsername(){
		WebElement user = Utils.waitToBePresent(driver, 1, By.id("username"));
		return user;
	}
	
	public void setUsername(String username){
		WebElement usernameValue = this.getUsername();
		usernameValue.clear();
		usernameValue.sendKeys("admin");
	}
	
	public WebElement getPassword(){
		WebElement pass = Utils.waitToBePresent(driver, 1, By.id("password"));
		return pass;
	}
	
	public void setPassword(String username){
		WebElement passwordValue = this.getPassword();
		passwordValue.clear();
		passwordValue.sendKeys("admin");
	}
	
	
	public void clickSignIn(){
		driver.findElement(By.cssSelector("form button")).click();
	}
	
	
	//istu ovakvu metodu za popunjavanje podataka new teacher
	public void signIn(String username, String password){
		this.setUsername(username);
		this.setPassword(password);
		this.clickSignIn();
	}
	

}
