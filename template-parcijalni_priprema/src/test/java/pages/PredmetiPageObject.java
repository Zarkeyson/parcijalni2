package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PredmetiPageObject {
WebDriver driver;
	
	public PredmetiPageObject(WebDriver driver){
		super();
		this.driver = driver;
	}
	
	public WebElement btnCreatePredmet(){
		WebElement btn = Utils.waitToBeVisible(driver, 2, By.xpath("//button[@ui-sref='predmeti.new']"));
		return btn;
	}
	
	public void clickBtn(){
		Utils.waitToBeVisible(driver, 2, By.xpath("//button[@ui-sref='predmeti.new']")).click(); //proveriti ovo kod drugih klasa da li mi je dobro posto sam kopirao
	}
	
	
}
