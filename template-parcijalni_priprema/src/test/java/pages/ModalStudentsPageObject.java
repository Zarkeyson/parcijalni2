package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ModalStudentsPageObject {
	WebDriver driver;
	
	public ModalStudentsPageObject(WebDriver driver){
		super();
		this.driver = driver;
	}
	
	public WebElement modalWindow(){
		WebElement modal = Utils.waitToBeVisible(driver, 2, By.className("modal-content"));
		return modal;
	}
	
	public WebElement getIndeks(){
		WebElement indeks = Utils.waitToBeVisible(driver, 1, By.id("indeks"));
		return indeks;
	}
	
	public void setIndeks(String value){
		WebElement name = this.getIndeks();
		name.sendKeys(value);
	}
	
	public WebElement getIme(){
		WebElement ime = Utils.waitToBeVisible(driver, 1, By.id("ime"));
		return ime;
	}
	
	public void setIme(String value){
		WebElement ime = this.getIme();
		ime.sendKeys(value);
	}
	//what is problem
	public WebElement getPrezime(){
		WebElement prezime = Utils.waitToBeVisible(driver, 1, By.id("prezime"));
		return prezime;
	}
	
	public void setPrezime(String value){
		WebElement prezime = this.getPrezime();
		prezime.sendKeys(value);
	}
	
	public WebElement getZvanje(){
		WebElement zvanje = Utils.waitToBeVisible(driver, 1, By.id("zvanje"));
		return zvanje;
	}
	
	public void setZvanje(String value){
		WebElement zvanje = this.getZvanje();
		zvanje.sendKeys("Profesor strucnog testiranja");
	}
	
	public WebElement getGrad(){
		WebElement grad = Utils.waitToBeVisible(driver, 1, By.id("grad"));
		return grad;
	}
	
	public void setGrad(String value){
		WebElement grad = this.getGrad();
		grad.sendKeys(value);
	}
	
	//sada i btn clic
	public void clickSave(){
		driver.findElement(By.xpath("//div[@class='modal-footer']/button[2]")).click(); //ovde eventualno uzeti onako preko cssselectora
	}
	
	//sada mogu onu metodu za popunjavanje odjednom
	public void createNewStudent(String indeks, String ime, String prezime, String grad){
		this.setIndeks(indeks);
		this.setIme(ime);
		this.setPrezime(prezime);
		this.setGrad(grad);
		this.clickSave();
	}
}
