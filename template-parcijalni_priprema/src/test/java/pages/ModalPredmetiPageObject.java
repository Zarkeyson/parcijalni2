package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

	public class ModalPredmetiPageObject {
		WebDriver driver;
	
		public ModalPredmetiPageObject(WebDriver driver){
			super();
			this.driver = driver;
		}
		
		
		//ovo nam izgleda ne treba
		public WebElement modalWindow(){
			WebElement modal = Utils.waitToBeVisible(driver, 2, By.className("modal-content"));
			return modal;
		}
		
		//naziv predmeta
		public WebElement getSubject(){
			WebElement we = driver.findElement(By.id("field_naziv"));
			return we;
		}
		
		//setuj ime predmeta
		public void setSubject(String value){
			//prvo napravimo taj WebElement!!!
			WebElement naziv = this.getSubject();
			naziv.sendKeys(value);
		}
		
		//ovo je seect za jednog studenta... ispod je multi select
		public void selectStudent(String studentName){
			Select studentDropDown = new Select(driver.findElement(By.name("studenti")));
			studentDropDown.selectByVisibleText(studentName);
		}
		
		public void multiSelectStudents(String[] selectStudents){
			Select studentDropDown = new Select(driver.findElement(By.name("studenti")));
			for (int i = 0; i < selectStudents.length; i++) {
				String studentName = selectStudents[i];
				studentDropDown.selectByVisibleText(studentName);
			}
		}
		
		//selektovanje jednog nastavnika
		public void selectTeacher(String teacherName){
			Select teacherDropDown = new Select(driver.findElement(By.name("nastavnici")));
			teacherDropDown.selectByVisibleText(teacherName);
		}
		
		//button save(samo nadjemo taj webelement sa klikom odma...)
		public void clickSaveSubject() {
			driver.findElement(By.xpath("//span[@translate='entity.action.save']")).click();
		}
		
		//popunjavanje polja
		public void popuniPolja(String naziv, String[] studentsName, String teacher){
			this.setSubject(naziv);
			this.multiSelectStudents(studentsName);
			this.selectTeacher(teacher);
			this.clickSaveSubject();
		}
			
	
}
