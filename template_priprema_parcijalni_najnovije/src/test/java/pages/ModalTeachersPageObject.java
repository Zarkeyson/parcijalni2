package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ModalTeachersPageObject {
	WebDriver driver;

	public ModalTeachersPageObject(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement modalWindow(){
		WebElement modal = Utils.waitToBeVisible(driver, 2, By.className("modal-content"));
		return modal;
	}
	
	public WebElement getName(){
		WebElement name = Utils.waitToBeVisible(driver, 1, By.id("ime"));
		return name;
	}
	
	public void setName(String value){
		WebElement name = this.getName();
		name.sendKeys("Milan");
	}
	
	public WebElement getPrezime(){
		WebElement prezime = Utils.waitToBeVisible(driver, 1, By.id("prezime"));
		return prezime;
	}
	
	public void setPrezime(String value){
		WebElement prezime = this.getPrezime();
		prezime.sendKeys("Milankovic");
	}
	
	public WebElement getZvanje(){
		WebElement zvanje = Utils.waitToBeVisible(driver, 1, By.id("zvanje"));
		return zvanje;
	}
	
	public void setZvanje(String value){
		WebElement zvanje = this.getZvanje();
		zvanje.sendKeys("Profesor strucnog testiranja");
	}
	
	//sada i btn clic
	public void clickSave(){
		driver.findElement(By.xpath("//div[@class='modal-footer']/button[2]")).click();
	}
	
	//sada mogu onu metodu za popunjavanje odjednom
	public void createNewTeacher(String ime, String prezime, String zvanje){
		this.setName(ime);
		this.setPrezime(prezime);
		this.setZvanje(zvanje);
		this.clickSave();
	}
}
