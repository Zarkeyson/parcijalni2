package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePageObject {
	WebDriver driver;
	
	public HomePageObject(WebDriver driver){
		this.driver = driver;
	}
	
	//nauciti i fail login!
	//u utils proveriti za bilo sta broj redova(njena metoda, sa njenih resenja)
	//odvojiti nastavniciPage i nastavnici ModalPage isto i za studente a verovatno i za predmete...
	
	public String getLoginMessage(){
		WebElement loginMssg = this.driver.findElement(By.xpath("//div[@ng-switch='IsAuthenticated()']"));
		return loginMssg.getText();
	}
}
