package pages;

import java.util.NoSuchElementException;

import javax.swing.tree.ExpandVetoException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	//kao i svugde izgleda prvo inicijalizuje WebDriver
	private WebDriver driver;
	
	public BasePage(WebDriver driver){
		super();
		this.driver = driver;
	}
	
	
	//metoda koja proverava da li je naslov isti kao prosledjeni
	public boolean presenceOfTitle(String title){
		boolean isPresent = new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs(title));
		return isPresent;
	}
	
	//metoda koja proverava URL isto kao i prethodna metoda samo kraci nacin
	public boolean presenceOfURL(String url){
		return new WebDriverWait(driver, 10).until(ExpectedConditions.urlToBe(url));
	}	
	
	//metoda koja vraca da je modalni dijalog vidljiv
	public WebElement getModalDialog(){
		return new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".modal-content")));
	}
	
	//sacekaj da bude klikabilan i onda ga i KLIKNI!!! 
	public void clickSaveEntity(){
		new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@translate='entity.action.save']"))).click();
	}
	
	//metoda sa dobijanje table entiteta
	public WebElement getTable(){
		return new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.className("jh-table")));
	}
	
	//broj redova u tabeli(posto moze da vrati i vise ide findElementssss i onda size...(don't forget!)
	public int numberOfElementsInTable(){
		return driver.findElements(By.xpath("//table/tbody/tr")).size();
	}
	
	//da li je element u tabeli(try/catch naredba
	public boolean isEntityInTable(String entity){
		try {
			driver.findElement(By.linkText(entity));
		} catch (NoSuchElementException e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}
	
	//vrati vrednost iz tabele po nekoj vrednosti(ovo pogledati sa gita ili nauciti napamet jbg
	public String getTextFromRowByValue(String value){
		return new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),\"" + value + "\")]/../.."))).getText();
	}
	

}
