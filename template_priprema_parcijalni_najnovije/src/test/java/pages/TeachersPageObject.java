package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TeachersPageObject {
	WebDriver driver;
	
	public TeachersPageObject(WebDriver driver){
		super();
		this.driver = driver;
	}
	
	public WebElement btnCreateTeacher(){
		WebElement btn = Utils.waitToBeVisible(driver, 2, By.xpath("//button[@ui-sref='nastavnici.new']"));
		return btn;
	}
	
	public void clickBtn(){
		Utils.waitToBeVisible(driver, 2, By.xpath("//button[@ui-sref='nastavnici.new']")).click();
	}
	
	//isto kao i za student efikasnija metoda koja radi obe stvari odjednom
	//u testu koristiti ovu metodu
	public void clickCreateNewTeacher(){
		WebElement btn = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ui-sref='nastavnici.new']")));
		btn.click();
	}
	
	//ovo nam ne treba! isto i u studeima
	public WebElement listTeacher(){
		WebElement table = Utils.waitToBeVisible(driver, 1, By.className("table-responsive"));
		return table;
	}
	
	
	
}
